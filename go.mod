module gitlab.com/kvgo/time_zone_gallery

go 1.15

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/hashicorp/vault/api v1.0.4
	github.com/satori/go.uuid v1.2.0
)
