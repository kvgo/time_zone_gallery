package main

import (
	"log"
	"os"
)

func jsTemplate(m map[string]string) {

	jsFile, _ := os.OpenFile("./templates/clock.js", os.O_RDWR, 0777)
	defer jsFile.Close()

	//escapedJS := template.JSEscapeString("javascript.tmpl")

	err := resourceTpl.ExecuteTemplate(jsFile, "javascript.tmpl", m)
	//err := resourceTpl.ExecuteTemplate(jsFile, "javascript.tmpl", m)
	if err != nil {
		log.Fatalln(err)
	}
}

func cssTemplate(m map[string]string) {

	cssFile, _ := os.OpenFile("./templates/template.css", os.O_RDWR, 0777)
	defer cssFile.Close()

	//Execute the template to create dynamic CSS file
	err := resourceTpl.ExecuteTemplate(cssFile, "css_template.tmpl", m)
	if err != nil {
		log.Fatalln(err)
	}
}
