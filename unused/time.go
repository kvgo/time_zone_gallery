package main

import (
	"strings"
	"time"
)

func convertTime(s string) map[string]string {
	//Set the time constants
	const RFC850 = "Monday, 02-Jan-06 15:04:05 MST"

	//Pass 's' the country/region into LoadLocation
	country, _ := time.LoadLocation(s)

	//Get the current time in country
	nowtime := time.Now()
	current := nowtime.In(country)

	//Getting the seconds using a different constant
	//Split the string into individual chars
	T2 := current.Format(RFC850)
	xT2 := strings.Split(T2, "")

	//Joining the xT1 slices at the indexes that represent the hour, minute and second

	//hour := strings.Join(xT2[21:23], "")
	//min := strings.Join(xT2[24:26], "")
	//sec := strings.Join(xT2[27:29], "")

	//hour := xT2[22]
	hour := strings.Join(xT2[20:22], "")
	//fmt.Println(hour)
	min := strings.Join(xT2[23:25], "")
	//fmt.Println(minute)
	sec := strings.Join(xT2[26:28], "")
	//fmt.Println(second)

	//returning the map of values
	return map[string]string{
		"hour": hour,
		"min":  min,
		"sec":  sec,
	}
}
