#This method uses multistage technique to build docker images
#Use an image to build the GO application
#Then copy the NEEDED files for the app, in this case all the HTML templates, iamges and other necessary files


##########
# Prep
##########
FROM golang:alpine3.12 AS build-env

WORKDIR /go/src/build_app/

# add the source
COPY . /go/src/build_app

##################
# Build Go Wrapper
##################

RUN GOOS=linux GOARCH=amd64 go build -o ./time_zone_gallery

###########################
#Package into runtime image
###########################
FROM golang:alpine3.12

WORKDIR /app

#Copy all the resources from the build stage to the new image 
COPY --from=build-env /go/src/build_app/time_zone_gallery /app
COPY --from=build-env /go/src/build_app/templates /app/templates
COPY --from=build-env /go/src/build_app/rome /app/rome
COPY --from=build-env /go/src/build_app/london /app/london
COPY --from=build-env /go/src/build_app/paris /app/paris
COPY --from=build-env /go/src/build_app/budapest /app/budapest
COPY --from=build-env /go/src/build_app/log /app/log

ENTRYPOINT ["./time_zone_gallery"]

EXPOSE 80
