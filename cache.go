package main

import (
	"context"
	"log"
	"os"
	"syscall"

	"github.com/go-redis/redis"
)

const (
	O_RDWR   int = syscall.O_RDWR
	O_APPEND int = syscall.O_APPEND
	O_WRONLY int = syscall.O_WRONLY
)

//Create the DB connection
//-var- is needed for global variables
var redisOptions = &redis.Options{
	Network: "tcp",
	//Redis DB address
	Addr: "127.0.0.1:6379",
	//Password: redisPassword(),
	DB: 0,
}

//Create the redis client to connect to redis
var redisClient = redis.NewClient(redisOptions)

//Create the context and connection timeout
//If the connection is not successful after 10 seconds
var ctx = context.Background()

//Open the log file for writing
//Throwing away the error for now(not good practice)
var dbLog, _ = os.OpenFile("log/db.log", O_WRONLY|O_APPEND, 0755)

//This function handles testing the redis db connection
//Exits if connection is unsuccessful
func dbConnection() {
	//We are deferring closing the log file
	//Throw a fatal error if it can't open the file
	//Print output to the log file
	defer dbLog.Close()
	log.SetOutput(dbLog)
	log.Println("Connecting to DB . . .")

	//Set the timeout for the checking if Redis is up/alive
	//timeOut, cancel := context.WithTimeout(ctx, 100*time.Second)

	//Check the connection, ping the redis IP
	//Print to the log file if the connection is successful or not
	//Throwing away the variable for ping, you have to use the result variable if its declared
	//_, err := redisClient.Ping(timeOut).Result()
	_, err := redisClient.Ping().Result()
	if err == nil {
		log.Println("CONNECTION SUCCESSFUL")
	}
	//defer cancel()

	//If the server is not available print the message and err to the log file
	//and Exit the program
	if err != nil {
		log.Println("REDIS DB NOT AVAILABLE", err)
		os.Exit(0)
	}
}

//This function sets the cookie into the redis cache
//Takes in a string of the cookie
func cacheCookie(cookie string) {
	//Set the cookie in a key/value format in redis db
	//err := redisClient.Set(ctx, "session", cookie, 5*time.Minute).Err()
	//if err != nil {
	//	log.Println(err)
	//}
	log.SetOutput(dbLog)
	defer dbLog.Close()

	//Set the cookies in a list
	//err := redisClient.RPush(ctx, "sessions", cookie).Err()
	err := redisClient.RPush("sessions", cookie).Err()
	if err != nil {
		log.Println(err)
	}
}
