package main

import (
	"log"
	"strings"

	//Using a named import for html/template and text/template
	//Using both packages at the same time
	htmltemplate "html/template"
	"net/http"
	texttemplate "text/template"
)

var htmlTpl *htmltemplate.Template
var resourceTpl *texttemplate.Template

//In func init parse the templates
//Check the redis connection, if it's not accessible exit the program
func init() {
	htmlTpl = htmltemplate.Must(htmltemplate.ParseGlob("templates/*.gohtml"))
	resourceTpl = texttemplate.Must(texttemplate.ParseGlob("templates/*.tmpl"))
	//dbConnection()
}

func main() {

	http.HandleFunc("/", index)
	http.HandleFunc("/about", about)
	http.HandleFunc("/budapest", location)
	http.HandleFunc("/london", location)
	http.HandleFunc("/paris", location)
	http.HandleFunc("/rome", location)

	http.Handle("/budapest/", http.StripPrefix("/budapest/", http.FileServer(http.Dir("./budapest"))))
	http.Handle("/london/", http.StripPrefix("/london/", http.FileServer(http.Dir("./london"))))
	http.Handle("/paris/", http.StripPrefix("/paris/", http.FileServer(http.Dir("./paris"))))
	http.Handle("/rome/", http.StripPrefix("/rome/", http.FileServer(http.Dir("./rome"))))
	http.Handle("/templates/", http.StripPrefix("/templates/", http.FileServer(http.Dir("./templates"))))
	http.Handle("favicon.ico", http.NotFoundHandler())

	http.ListenAndServe(":80", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	//Set the cookie
	//http.SetCookie(w, generateCookie(req))
}

func about(w http.ResponseWriter, req *http.Request) {
	//http.SetCookie(w, generateCookie(req))

	urlPath := (req.URL.Path)

	tabTitle := strings.Replace(strings.ToUpper(urlPath), "/", "", 1)

	titleMap := map[string]string{
		"tabTitle":  tabTitle,
		"aboutText": "World Clock A simple website written in Go to serve random images from different countries around the world",
	}

	err := htmlTpl.ExecuteTemplate(w, "about.gohtml", titleMap)
	if err != nil {
		log.Fatalln(err)
	}
}

//This generates and calls the function that stores the cookie in the DB
//func generateCookie(mainReq *http.Request) *http.Cookie {
//	cookie, err := mainReq.Cookie("session")
//	if err != nil {
//		//Get a new UUID
//		sID := uuid.NewV4()
//		cookie = &http.Cookie{
//			Name: "session",
//			//Set the sID to String
//			Value: sID.String(),
//		}
//	}
//
//	//Passing the value of cookie into cacheCookie
//	//cacheCookie stores it in the db
//	cacheCookie(cookie.Value)
//
//	return cookie
//}
