package main

import (
	"log"
	"os"

	vault "github.com/hashicorp/vault/api"
)

func redisPassword() string {

	var vaultLog, _ = os.OpenFile("log/vault.log", O_WRONLY|O_APPEND, 0755)
	defer vaultLog.Close()
	log.SetOutput(vaultLog)

	vaultConfig := &vault.Config{
		Address: "http://52.149.231.214:8200",
		//Address: os.Getenv("VAULT_ADDR"),
	}

	vaultClient, err := vault.NewClient(vaultConfig)
	if err != nil {
		log.Println(err)
	}

	vaultClient.SetToken("s.WsYQSr4Dk5WWAb3F1PdRohkJ")

	secret, err := vaultClient.Logical().Read("secret/data/time_zone_gallery/redis")
	if err != nil {
		log.Println(err)
	}

	//this is type assertion
	// Data is the actual contents of the secret. The format of the data
	// is arbitrary and up to the secret backend.
	password := secret.Data["data"].(map[string]interface{})["password"].(string)
	return password
}
