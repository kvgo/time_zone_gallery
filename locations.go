package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
)

func cssFile(location string) string {

	//Read the files from the directory
	dir, err := os.Open("." + location)
	if err != nil {
		fmt.Println(err)
	}
	defer dir.Close()

	files, err := dir.Readdirnames(-1)
	if err != nil {
		fmt.Println(err)
	}

	//--Randomizer
	//Get the length of the files slice
	//Choose
	//Print the index of 'files' slice passing in the randomNumber as the index
	if len(files) > 0 {
		randomPic := files[rand.Intn(len(files))]
		return ".." + location + "/" + randomPic
	}

	return ".." + location + "/"
}
func chooseTZ(location string) string {
	//Set the TimeZone(TZ) based on URL
	//Using the IANA timeZone database
	formatLoctitle := strings.Title(strings.ToLower(location))
	return "Europe/" + formatLoctitle
}

func location(w http.ResponseWriter, req *http.Request) {
	//w.Header().Set("Content-Type", "image/jpeg; charset=utf-8")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	urlPath := (req.URL.Path)
	css := cssFile(urlPath)
	TZ := chooseTZ(urlPath)
	fmt.Println(css)
	fmt.Println(TZ)

	//Get the country location by stripping the "/" from the path
	//converting to caps for the title as seen on the tab in a browser
	locTitle := strings.Replace(strings.ToUpper(urlPath), "/", "", 1)

	//Create a map to access the data accurately
	resourceTemplatemap := map[string]string{
		"locationTitle": locTitle,
		//"imageFile":     randomFilePath,
		//adding the "dot" to make the CSS path syntatically correct
		"css":      css,
		"timeZone": TZ,
	}

	cssFile, _ := os.OpenFile("./templates/template.css", os.O_RDWR, 0777)
	defer cssFile.Close()
	//Execute the template to create dynamic CSS file
	err := resourceTpl.ExecuteTemplate(cssFile, "css_template.tmpl", resourceTemplatemap)
	if err != nil {
		log.Fatalln(err)
	}

	jsFile, _ := os.OpenFile("./templates/clock.js", os.O_RDWR, 0777)
	err = resourceTpl.ExecuteTemplate(jsFile, "javascript.tmpl", resourceTemplatemap)
	if err != nil {
		log.Fatalln(err)
	}
	jsFile.Close()

	err = htmlTpl.ExecuteTemplate(w, "index.gohtml", resourceTemplatemap)
	if err != nil {
		log.Fatalln(err)
	}
}
