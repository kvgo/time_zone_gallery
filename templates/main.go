package main

import (
	"fmt"
	"log"
	"os"
	"text/template"
)

var resourceTpl *template.Template

func init() {
	resourceTpl = template.Must(template.ParseGlob("*.tmpl"))
}

func main() {

	commits := map[string]string{
		"rsc": "711",
		"r":   "2138",
		"gri": "1908",
		"adg": "912",
	}

	jsTemplate(commits)
	//fmt.Println(commits)

}

func jsTemplate(m map[string]string) {

	jsFile, _ := os.OpenFile("../templates/clock.js", os.O_RDWR, 0777)
	defer jsFile.Close()
	fmt.Println(m)

	escapedJS := template.JSEscapeString("javascript.tmpl")
	fmt.Println(escapedJS)

	err := resourceTpl.ExecuteTemplate(jsFile, "javascript.tmpl", m)
	if err != nil {
		log.Fatalln(err)
	}
}
