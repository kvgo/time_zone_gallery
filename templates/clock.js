import { DateTime } from "./js_modules/luxon.js";


function currentTime() {
  var localTime = DateTime.local()
  var foreignTime  = DateTime.local().setZone("Europe//Paris");
   
    var date = new Date(); /* creating object of Date class */
    var hour = foreignTime.hour
    var min = foreignTime.minute
    var sec =  foreignTime.second
    var midday = "AM";
    
    midday = (hour >= 12) ? "PM" : "AM"; /* assigning AM/PM */
    hour = (hour == 0) ? 12 : ((hour > 12) ? (hour - 12): hour); /* assigning hour in 12-hour format */
    hour = updateTime(hour);
    min = updateTime(min);
    sec = updateTime(sec);
    document.getElementById("clock").innerHTML = hour + " : " + min + " : " + sec + " " + midday; /* adding time to the div */
    var t = setTimeout(currentTime, 1000); /* setting timer */
  }
  
  function updateTime(k) { /* appending 0 before time elements if less than 10 */
    if (k < 10) {
      return "0" + k;
    }
    else {
      return k;
    }
  }
  
  currentTime();
  
  //This comment is to counter a "bug"
  //The last chars in the template get weirdird