package main

import (
	"testing"
)

func TestCssFile(t *testing.T) {
	//Create a slice of anonymous structs containing the test case name
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "CSS_Path_Syntax",
			input:    "/sydney",
			expected: "../sydney/",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			loc := cssFile(tt.input)
			if loc != tt.expected {
				t.Errorf("want%q; got%q", tt.expected, loc)
			}
		})
	}

}

func TestChooseTZ(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "JS_TimeZone_Syntax",
			input:    "/sydney",
			expected: "Europe/Sydney",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tz := chooseTZ(tt.input)
			if tz != tt.expected {
				t.Errorf("want%q; got%q", tt.expected, tz)
			}
		})
	}
}
