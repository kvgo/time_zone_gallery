package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"syscall"
	"time"

	"github.com/go-redis/redis"
	uuid "github.com/satori/go.uuid"
)

const (
	O_RDWR   int = syscall.O_RDWR
	O_APPEND int = syscall.O_APPEND
	O_WRONLY int = syscall.O_WRONLY
)

func init() {
	dbLog, err := os.OpenFile("log/db.log", O_WRONLY|O_APPEND, 0755)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer dbLog.Close()
	log.SetOutput(dbLog)
	log.Println("Connecting to DB . . .")

	//Create the DB connection
	redisOptions := &redis.Options{
		Network:  "tcp",
		Addr:     "127.0.0.1:6379",
		Password: "joelaine",
		DB:       0,
	}

	redisClient := redis.NewClient(redisOptions)
	//Create the context and connection timeout
	ctx := context.Background()
	timeOut, cancel := context.WithTimeout(ctx, 9*time.Second)

	ping, err := redisClient.Ping(timeOut).Result()
	log.Println("Connection successful", ping)
	defer cancel()

	if err != nil {
		log.Println("REDIS DB NOT AVAILABLE", err)
		os.Exit(0)
	}
	fmt.Println(ping)
}

var ctx = context.Background()

func main() {

	//Open the DB log file for writing
	dbLog, err := os.OpenFile("log/db.log", O_WRONLY|O_APPEND, 0755)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer dbLog.Close()
	log.SetOutput(dbLog)

	//Create the DB connection
	redisOptions := &redis.Options{
		Network:  "tcp",
		Addr:     "127.0.0.1:6379",
		Password: "joelaine",
		DB:       0,
	}
	redisClient := redis.NewClient(redisOptions)

	sID := createSession()

	err = redisClient.Set(ctx, "session", sID, 10*time.Second).Err()
	if err != nil {
		log.Println(err)
	}

	val, err := redisClient.Get(ctx, "session").Result()
	if err != nil {
		log.Println(err)
	}
	fmt.Println("key", val)

	val2, err := redisClient.Get(ctx, "key2").Result()
	if err == redis.Nil {
		log.Println("key2 does no exist", err)
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key2", val2)
	}
}

func createSession() string {

	sID, _ := uuid.NewV4()
	value := sID.String()
	return value
}
