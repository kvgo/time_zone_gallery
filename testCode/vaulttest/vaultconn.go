package main

import (
	"fmt"

	vault "github.com/hashicorp/vault/api"
)

var vaultConfig = &vault.Config{
	Address: "http://52.149.231.214:8200",
}

var vaultClient, _ = vault.NewClient(vaultConfig)

func main() {
	vaultClient.SetToken("s.WsYQSr4Dk5WWAb3F1PdRohkJ")

	secret, err := vaultClient.Logical().Read("secret/data/time_zone_gallery/redis")
	if err != nil {
		fmt.Println(err)
	}

	//Secret returns a *Secret which is a struct
	//use <variable>.<map_name> to access the first level map
	//use type assertion to access the nested map
	fmt.Printf("%T\n", secret)
	fmt.Println(secret)
	fmt.Println("====================")

	fmt.Println(secret.Data)
	fmt.Printf("%T\n", secret.Data)
	fmt.Println("====================")

	password := secret.Data["data"].(map[string]interface{})["password"]
	fmt.Printf("%T:", password)
	fmt.Println(password)

	fmt.Println(secret.Data["data"])
	fmt.Printf("%T\n", secret.Data["data"])
	fmt.Println("====================")

	fmt.Println(secret.Data["metadata"])
	fmt.Printf("%T\n", secret.Data["metadata"])

}
