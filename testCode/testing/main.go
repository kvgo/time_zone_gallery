package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"text/template"
	"time"
)

var htmlTpl *template.Template
var resourceTpl *template.Template

func init() {
	htmlTpl = template.Must(template.ParseGlob("templates/*.gohtml"))
	resourceTpl = template.Must(template.ParseGlob("templates/*.tmpl"))
}

func main() {

	TZ := "Europe/Rome"

	fmt.Println(convertTime(TZ))

	commits := map[string]string{
		"rsc": "711",
		"r":   "2138",
		"gri": "1908",
		"adg": "912",
	}

	jsTemplate(commits)

}

func convertTime(s string) map[string]string {

	//randomFilePath := "Rome"
	//TZ := "Europe/" + randomFilePath
	fmt.Println("THIS IS TZ: ", s, "\n")

	const RFC850 = "Monday, 02-Jan-06 15:04:05 MST"
	TZz, _ := time.LoadLocation(s)

	nowtime := time.Now()
	current := nowtime.In(TZz)

	//fmt.Println("THIS IS NY TIME: ", nowtime)
	//fmt.Println("THIS IS ROME TIME: ", current, "\n")
	//fmt.Println("THIS IS NY TIME SIMPLE: ", nowtime.Format(Kitchen))
	//fmt.Println("THIS IS ROME TIME SIMPLE: ", current.Format(Kitchen), "\n")
	//fmt.Println("THIS IS NY TIME LONG_FORM: ", nowtime.Format(RFC850))
	//fmt.Println("THIS IS ROME TIME LOMG_FORM: ", current.Format(RFC850))

	//T1 := "0" + current.Format(Kitchen)
	//fmt.Println(T1)

	//xT1 := strings.Split(T1, "")
	//fmt.Println(xT1)
	//fmt.Printf("%T\n", xT1[0:2])

	T2 := current.Format(RFC850)
	//fmt.Println(T2)

	xT2 := strings.Split(T2, "")
	fmt.Println(xT2)

	//hour := xT2[22]
	hour := strings.Join(xT2[20:22], "")
	//fmt.Println(hour)
	minute := strings.Join(xT2[23:25], "")
	//fmt.Println(minute)
	second := strings.Join(xT2[26:28], "")
	//fmt.Println(second)

	return map[string]string{
		"hour": hour,
		"min":  minute,
		"sec":  second,
	}
}

func jsTemplate(m map[string]string) {

	jsFile, _ := os.OpenFile("./templates/clock.js", os.O_RDWR, 0777)
	defer jsFile.Close()

	escapedJS := template.JSEscapeString("javascript.tmpl")
	fmt.Println(escapedJS)
	err := resourceTpl.ExecuteTemplate(jsFile, "javascript.tmpl", m)
	//err := resourceTpl.ExecuteTemplate(jsFile, "javascript.tmpl", m)
	if err != nil {
		log.Fatalln(err)
	}
}
